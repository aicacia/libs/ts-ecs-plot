import { WebCanvas } from "@aicacia/ecs-game/lib/web";
import { PlotSceneBuilder } from "../PlotSceneBuilder";
export declare class WebPlotSceneBuilder extends PlotSceneBuilder {
    constructor(canvas: WebCanvas);
}
