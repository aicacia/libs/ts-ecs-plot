# ts-ecs-plot

[![license](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue")](LICENSE-MIT)
[![docs](https://img.shields.io/badge/docs-typescript-blue.svg)](https://aicacia.gitlab.io/libs/ts-ecs-plot/)
[![npm (scoped)](https://img.shields.io/npm/v/@aicacia/ecs-plot)](https://www.npmjs.com/package/@aicacia/ecs-plot)
[![pipelines](https://gitlab.com/aicacia/libs/ts-ecs-plot/badges/master/pipeline.svg)](https://gitlab.com/aicacia/libs/ts-ecs-plot/-/pipelines)

a 2d plotting lib for ecs
